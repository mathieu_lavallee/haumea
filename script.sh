#!/bin/bash

# Efface les fichiers intermédiaires et le JAR final
rm -rf bin/*
rm -rf install/*

# Compilation de la classe
mkdir -p bin/spacefacts
javac src/spacefacts/LabelInfo.java -d bin

# Compilation du main
mkdir -p bin/spacefacts
javac src/spacefacts/Haumea.java src/spacefacts/LabelInfo.java -d bin

# Construction du JAR
# -- Salut ! Attention avec le make et la commande "cd". Il faut appeler les commandes d'une manière spéciale. Va lire la réponse de falstro au https://stackoverflow.com/questions/1789594/how-do-i-write-the-cd-command-in-a-makefile Bonne chance, -Paula Bean.
cd bin
mkdir -p img
mkdir -p content
yes | cp -rf ../src/img/haumea.jpg img/haumea.jpg
yes | cp -rf ../src/content/haumea.htm content/haumea.htm
jar cmvf ../src/META-INF/MANIFEST.MF Haumea.jar spacefacts/Haumea* spacefacts/LabelInfo* img/* content/*
cd ..
mkdir -p install
yes | cp -rf bin/Haumea.jar install/Haumea.jar

# Exécute le JAR complété
java -jar install/Haumea.jar





