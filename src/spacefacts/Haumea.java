/*
 * Example adapted from https://docs.oracle.com/javase/tutorial/uiswing/examples/start/HelloWorldSwingProject/src/start/HelloWorldSwing.java  
 */

package spacefacts;

// Standard libraries
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.StringBuilder;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;

// Custon classes
import spacefacts.LabelInfo;

public class Haumea {
    /**
     * Create the GUI and show it. For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("Haumea");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());

	    // Create the main panel
	    JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());

        // Add the name label.
        JLabel name = new JLabel("Haumea");
        panel.add(name);

	    // Add the picture label.
	    try {
		    //ImageIcon image = new ImageIcon(ImageIO.read(new File("img/haumea.jpg")));	
		    ImageIcon image = new ImageIcon(ImageIO.read(Haumea.class.getClassLoader().getResource("img/haumea.jpg")));
		    JLabel icon = new JLabel(image);
		    panel.add(icon);
	    } catch (IOException ex) {ex.printStackTrace();}

        // Add the information label.
        JLabel info = new LabelInfo();
        panel.add(info);

	    // Add the button.
	    JButton button = new JButton("Fascinating!");
	    button.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
			    System.exit(0);
		    }
	    });
        panel.add(button);

	    // Add the content to the frame
	    frame.add(panel);

        // Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Main static function.
     * */
    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}

