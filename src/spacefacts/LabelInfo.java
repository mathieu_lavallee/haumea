/*
 * Custom class for the label.
 */
package spacefacts;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.StringBuilder;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Scanner;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class LabelInfo extends JLabel {
    
    public LabelInfo() {
        this.addContent();
	    //panel.add(information_label, BorderLayout.SOUTH);
    }

    private void addContent() {
        // Add the information label.
	    StringBuilder contentBuilder = new StringBuilder();
	    try {
            URL res = LabelInfo.class.getClassLoader().getResource("content/haumea.htm");
            BufferedReader in = new BufferedReader(
                new InputStreamReader(res.openStream())
            );

            String inputLine;
            while ((inputLine = in.readLine()) != null)
                contentBuilder.append(inputLine);
            in.close();
        } catch (IOException ex) {ex.printStackTrace();}

	    String content = contentBuilder.toString();
	    this.setText(content);
	}
}

